package com.example.mdvk1.wite.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.mdvk1.wite.R;

public class ScrollingActivity extends AppCompatActivity {

    ImageButton back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ImageButton back = (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(ScrollingActivity.this, MainActivity.class);
                startActivity(back);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uri1 = "https://belajarsamatemix.blogspot.com/p/landing.html";
                Intent bukaWeb1 = new Intent(ScrollingActivity.this, WebBrowser.class);
                bukaWeb1.putExtra("URL", uri1);
                startActivityForResult(bukaWeb1, 1);
            }
        });
    }
}
