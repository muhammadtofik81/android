/*
 *   ~ Copyright (c) 2017. MDVK adalah sebuah aplikasi abal-abal yang dibangun atas keisengan semata :D
 *   ~ Umumnya kegunaan aplikasi MDVK adalah untuk membuat Perangkat kamu terhubung ke(@ wifi.id)
 *   ~ secara gratis tanpa harus membeli voucher atau dsb.
 *   ~
 *   ~ Aplikasi Ini dibuat oleh : Aditya Pratama , pada tanggal : 6/14/2016
 *   ~
 *   ~
 */

package com.example.mdvk1.wite.views;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.easyandroidanimations.library.BounceAnimation;
import com.example.mdvk1.wite.R;
import com.example.mdvk1.wite.model.MDVK;

public class SplashScreen extends MDVK {
    /*
         todo (✓) :
         todo ( ) :
     */
    private LinearLayout lv_loading;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Getting Windows ready for fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        lv_loading = (LinearLayout) findViewById(R.id.lv_loading);

//        //pembuatan animasi Bounce (atas bawahh)
//        new BounceAnimation(lv_loading)
//                .setBounceDistance(50)
//                .setDuration(3000)
//                .animate();

        taskDone(3000);
    }


    public void taskDone(int waitingTime) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                prinT(" @ Start Application ");
                Intent start = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(start);
                finish();
            }
        }, waitingTime);

    }
}