/*
 *   ~ Copyright (c) 2017. MDVK adalah sebuah aplikasi abal-abal yang dibangun atas keisengan semata :D
 *   ~ Umumnya kegunaan aplikasi MDVK adalah untuk membuat Perangkat kamu terhubung ke(@ wifi.id)
 *   ~ secara gratis tanpa harus membeli voucher atau dsb.
 *   ~
 *   ~ Aplikasi Ini dibuat oleh : Aditya Pratama , pada tanggal : 6/14/2016
 *   ~
 *   ~
 */

package com.example.mdvk1.wite.model;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;


@SuppressLint("Registered")
public class MDVK extends AppCompatActivity {
    Dialog myDialog;


    public void prinT(String msg) {
        System.out.println("-> " + msg);
    }

    public void SH_TOAST(String query) {
        Toast.makeText(MDVK.this, query, Toast.LENGTH_SHORT).show();
    }

    public void SH_TOAST2(String query) {
        Toast.makeText(MDVK.this, query, Toast.LENGTH_LONG).show();
    }

    public void SH_ALERT_POLOS(String judul, String query) {
        AlertDialog alertDialog = new AlertDialog.Builder(MDVK.this).create();
        alertDialog.setTitle(judul);
        alertDialog.setCancelable(false);
        alertDialog.setMessage(query);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }




}

