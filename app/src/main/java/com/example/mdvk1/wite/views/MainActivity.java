package com.example.mdvk1.wite.views;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.text.InputType;
import android.text.format.Formatter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.mdvk1.wite.R;
import com.example.mdvk1.wite.controller.Constans;
import com.example.mdvk1.wite.model.MDVK;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends MDVK {
    // Init
    Button btnConnect, btnAbout, btnFollow, btnSetup, btnexit, btnIpAddress, akun;
    TextView tvTask, tvStatus, tvCurrentAccount, tvsub;
    TextView tvSSID, tvBSSID;
    RadioGroup rg1;
    RadioButton rb1, rb2, rb3, rb4, rb5, rb6, rb7, rb8, rb9, rb10, rb11, rb12, rb13, rb14, rb15;
    ProgressBar pb1;
    OkHttpClient client;

    String getGwidFromLocal = "myGWID", name, m_gwid = "", m_ip = "";


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        client = new OkHttpClient();

        deklarasiKomponen();
        gettingNetworkInfo();
        readGWID();
        pilihAkun();


        tvsub.setText("Muhammad Tofik");
        // Aksi
        btnSetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Setup GWID Manual");

                // Set up the input
                final EditText input = new EditText(MainActivity.this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        m_gwid = input.getText().toString();
                        btnSetup.setText(m_gwid);
                        saveGWID();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        readGWID();
                    }
                });

                builder.show();
            }
        });
//                               Setup Ip Manual
        btnIpAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Setup IP Manual");

                // Set up the inputke
                final EditText input = new EditText(MainActivity.this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        m_ip = input.getText().toString();
                        btnIpAddress.setText(m_ip);

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                    }
                });

                builder.show();
            }
        });

        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strGWID = btnSetup.getText().toString();

                if (btnConnect.getText().toString().contains("Test Speed")) {
                    String uri1 = "http://temix.speedtestcustom.com";
                    Intent bukaWeb1 = new Intent(MainActivity.this, WebBrowser.class);
                    bukaWeb1.putExtra("URL", uri1);
                    startActivityForResult(bukaWeb1, 1);
                } else {
                    if (strGWID.isEmpty() || strGWID.contains("GWID")) {
                        SH_ALERT_POLOS("UPS", "GWID Masih Kosong, Silahkan Setup GWID Terlebih Dahulu~");
                        btnSetup.callOnClick();

                    } else {
                        btnConnect.setVisibility(View.GONE);
                        new GetTask().execute();
                    }
                }

            }
        });

        btnAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent abOut = new Intent(MainActivity.this, ScrollingActivity.class);
                startActivity(abOut);
            }
        });

        akun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent abOut = new Intent(MainActivity.this, spin.class);
//                startActivity(abOut);
                
                String uril = "https://madavaka.me/temp/r.php";
                Intent bukaWeb1 = new Intent(MainActivity.this, WebBrowser.class);
                bukaWeb1.putExtra("URL", uril);
                startActivityForResult(bukaWeb1, 1);
            }
        });

        btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri1 = "https://www.instagram.com/tofik_muhammad10/";
                Intent bukaWeb1 = new Intent(MainActivity.this, WebBrowser.class);
                bukaWeb1.putExtra("URL", uri1);
                startActivityForResult(bukaWeb1, 2);
            }
        });
        btnexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri1 = "http://welcome2.wifi.id/authnew/logout/logoutx.php";
                Intent bukaWeb2 = new Intent(MainActivity.this, WebBrowser.class);
                bukaWeb2.putExtra("URL", uri1);
                startActivityForResult(bukaWeb2, 3);
            }
        });

    }

    private void deklarasiKomponen() {
        btnConnect = findViewById(R.id.btnConnect);
        btnAbout = findViewById(R.id.btnAbout);
        btnFollow = findViewById(R.id.btnFollow);
        btnSetup = findViewById(R.id.btnSetUp);
        btnexit = findViewById(R.id.btnexit);

        tvTask = findViewById(R.id.tvTask);
        tvStatus = findViewById(R.id.tvStatus);
        tvCurrentAccount = findViewById(R.id.tvCurrentUsername);
        tvsub = findViewById(R.id.tvsub);
        akun = findViewById(R.id.akun);

//        tvGwid = findViewById(R.id.tvGwid);
        btnIpAddress = findViewById(R.id.tvIpAddress);
        tvSSID = findViewById(R.id.tvSSID);
        tvBSSID = findViewById(R.id.tvBSSID);

        rg1 = findViewById(R.id.rg1);
        rb1 = findViewById(R.id.rb1);
        rb2 = findViewById(R.id.rb2);
        rb3 = findViewById(R.id.rb3);
        rb4 = findViewById(R.id.rb4);
        rb5 = findViewById(R.id.rb5);
        rb6 = findViewById(R.id.rb6);
        rb7 = findViewById(R.id.rb7);
        rb8 = findViewById(R.id.rb8);
        rb9 = findViewById(R.id.rb9);
        rb10 = findViewById(R.id.rb10);
        rb11 = findViewById(R.id.rb11);
        rb12 = findViewById(R.id.rb12);
        rb13 = findViewById(R.id.rb13);
        rb14 = findViewById(R.id.rb14);
        rb15 = findViewById(R.id.rb15);

        pb1 = findViewById(R.id.pb1);

    }

    @SuppressLint("SetTextI18n")
    private void readGWID() {
        try {
            FileInputStream fin = openFileInput(getGwidFromLocal);
            InputStreamReader inputStreamReader = new InputStreamReader(fin);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            fin.close();
            inputStreamReader.close();
            btnSetup.setText("" + stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveGWID() {
        try {
            FileOutputStream fos = openFileOutput(getGwidFromLocal, Context.MODE_PRIVATE);
            name = btnSetup.getText().toString();
            fos.write(name.getBytes());
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void gettingNetworkInfo() {
        // GET Network Status
        WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        assert wifiMgr != null;
        WifiInfo wifiInfo = wifiMgr.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        String ipAddress = Formatter.formatIpAddress(ip);
        String ssid = wifiInfo.getSSID();
        String getbssid = wifiInfo.getBSSID();
        String mac = wifiInfo.getMacAddress();

        btnIpAddress.setText(ipAddress);
        tvSSID.setText(ssid);
        tvBSSID.setText(getbssid);
    }

    public void pilihAkun() {
        //Default
//        tvCurrentAccount.setText(Constans.AKUN_SATU);
        rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (checkedId == R.id.rb1) {
                    tvCurrentAccount.setText(Constans.AKUN_SATU);
                } else if (checkedId == R.id.rb2) {
                    tvCurrentAccount.setText(Constans.AKUN_DUA);
                } else if (checkedId == R.id.rb3) {
                    tvCurrentAccount.setText(Constans.AKUN_TIGA);
                } else if (checkedId == R.id.rb4) {
                    tvCurrentAccount.setText(Constans.AKUN_EMPAT);
                } else if (checkedId == R.id.rb5) {
                    tvCurrentAccount.setText(Constans.AKUN_LIMA);
                } else if (checkedId == R.id.rb6) {
                    tvCurrentAccount.setText(Constans.AKUN_ENAM);
                } else if (checkedId == R.id.rb7) {
                    tvCurrentAccount.setText(Constans.AKUN_TUJUH);
                } else if (checkedId == R.id.rb8) {
                    tvCurrentAccount.setText(Constans.AKUN_DELAPAN);
                } else if (checkedId == R.id.rb9) {
                    tvCurrentAccount.setText(Constans.AKUN_SEMBILAN);
                } else if (checkedId == R.id.rb10) {
                    tvCurrentAccount.setText(Constans.AKUN_SEPULUH);
                } else if (checkedId == R.id.rb11) {
                    tvCurrentAccount.setText(Constans.AKUN_SEBELAS);
                } else if (checkedId == R.id.rb12) {
                    tvCurrentAccount.setText(Constans.AKUN_DUABELAS);
                } else if (checkedId == R.id.rb13) {
                    tvCurrentAccount.setText(Constans.AKUN_TIGABELAS);
                } else if (checkedId == R.id.rb14) {
                    tvCurrentAccount.setText(Constans.AKUN_EMPATBELAS);
                } else if (checkedId == R.id.rb15) {
                    tvCurrentAccount.setText(Constans.AKUN_LIMABELAS);
                } else {
                    tvStatus.setText("Gagal Menghubungkan");
                    SH_ALERT_POLOS("UPS", "Anda belum memilih akun ~");
                }
            }
        });

    }

//
//    public void pilihAkun() {
//          //Default
//          //tvCurrentAccount.setText(Constans.AKUN_SATU);
//        btnConnect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String[] akun = {"username=98126577038110@spin2&password=shi", "username=98123345615078@spin2&password=kmi", "username=99217022446@spin2&password=rom", "username=98223742157933@spin2&password=upz", "username=98125334531470@spin2&password=zzr", "username=98128195327019@spin2&password=wsc", "username=98213837550811@spin2&password=hsc", "username=98123676395935@spin2&password=dfv"};
//
//                ArrayList<String> listAngka = new ArrayList<>(Arrays.asList(akun));
//
//                Collections.shuffle(listAngka);
//
//                System.out.println("Setelah di shuffle listAngka adalah: " + listAngka);
//
//            }
//        });
//
//    }
//}

    //     ==============================================================================
    //                             INNER CLASS -> Worker
    //     ==============================================================================

    public class GetTask extends AsyncTask<String, Void, String> {
        private Exception exception;


        String getRs(String url) throws IOException {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        }

        protected String doInBackground(String... params) {
            // prinT(current);
            try { // param is {ipc , gwid , mac .}
                prinT("Cuba Request ~");
                return getRs(
                        Constans._BASE_REQUEST +
                                "ipc=" + btnSetup.getText().toString() +
                                "gw_id=" + btnSetup.getText().toString() +
                                "&" + tvCurrentAccount.getText().toString()
                );
            } catch (Exception e) {
                this.exception = e;
                return null;
            }
        }

        @SuppressLint({"ResourceAsColor", "SetTextI18n"})
        protected void onPostExecute(String getResponse) {
//            prinT(getResponse);
//            tvTask.setText("... By TeMix ...");

            try {

                if (getResponse.contains("Sukses")) {
                    prinT("Login Sukses");
                    SH_TOAST2("LOGIN SUKSES, MET BROWSING >,< ");
                    tvTask.setText("Happy Browsing :)");
                    tvStatus.setText("Terhubung");
                    btnConnect.setVisibility(View.VISIBLE);
                    btnConnect.setText("Test Speed");
//                    btnConnect.setBackgroundColor(R.color.colorPrimaryDark);
                    pb1.setVisibility(View.INVISIBLE);
                    btnIpAddress.setVisibility(View.GONE);

                    //String uri1 = "http://mirorapalah.me/landing/";
                    String uri1 = "https://belajarsamatemix.blogspot.com/p/landing.html";
                    Intent bukaWeb1 = new Intent(MainActivity.this, WebBrowser.class);
                    bukaWeb1.putExtra("URL", uri1);
                    startActivityForResult(bukaWeb1, 1);

                } else if (getResponse.contains("No Session found")) {
                    prinT("No Session Found !");
                    SH_ALERT_POLOS("No Session Found ! (0x4)",
                            "Solusi : \n\n" +
                                    "Reconnect @wifi.id");
                    tvTask.setText("Process Stopped :( ");
                    btnConnect.setVisibility(View.VISIBLE);

                } else {
                    SH_TOAST("FAILED");
                    tvStatus.setText("Gagal Menghubungkan");
                    btnConnect.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                prinT(e.toString());
                prinT("RTO");
                tvStatus.setText("Process Stopped :( ");
                btnConnect.setVisibility(View.VISIBLE);
            }

        }
    }


}
