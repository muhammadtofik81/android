/*
 *   ~ Copyright (c) 2017. MDVK adalah sebuah aplikasi abal-abal yang dibangun atas keisengan semata :D
 *   ~ Umumnya kegunaan aplikasi MDVK adalah untuk membuat Perangkat kamu terhubung ke(@ wifi.id)
 *   ~ secara gratis tanpa harus membeli voucher atau dsb.
 *   ~
 *   ~ Aplikasi Ini dibuat oleh : Aditya Pratama , pada tanggal : 6/14/2016
 *   ~
 *   ~
 */

package com.example.mdvk1.wite.views;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.baoyz.widget.PullRefreshLayout;
import com.example.mdvk1.wite.R;
import com.example.mdvk1.wite.model.MDVK;

public class WebBrowser extends MDVK {
    WebView wb1;
    PullRefreshLayout swipeRefreshLayout;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_browser);
        setUpKom();

        Intent in = getIntent();
        String url = in.getStringExtra("URL");

        wb1.setWebViewClient(new MyBrowser());
        wb1.getSettings().setLoadsImagesAutomatically(true);
        wb1.getSettings().setJavaScriptEnabled(true);
        wb1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wb1.loadUrl(url);
//        SH_TOAST2("LOGIN SUKSES, MET BROWSING >,< ");

        PullRefreshLayout refreshLayout = (PullRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        refreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                wb1.reload();
            }
        });
// refresh complete
        refreshLayout.setRefreshing(false);
    }


    //set Up Kom
    public void setUpKom () {
        wb1 = findViewById(R.id.wb1);

        // Buat Tombol Back
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    // back Button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
